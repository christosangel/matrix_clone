# matrix_clone

A matrix clone in c language. 

During  running, the terminal can be resized.

## INSTRUCTIONS

*  From a terminal, compile the script running the command:

    gcc matrix_clone.c -Wall -o matrix_clone

* Then run the script:

    ./matrix_clone

* That's it!

![matrix.clone.gif](matrix_clone.gif)

* Following the instructions in the comments, you can adjust the speed of the letter cascade and the colors to your liking.
