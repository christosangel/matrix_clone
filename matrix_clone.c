//                      █                 ▗▄▖
//           ▐▌         ▀                 ▝▜▌
//▐█▙█▖ ▟██▖▐███  █▟█▌ ██  ▝█ █▘      ▟██▖ ▐▌   ▟█▙ ▐▙██▖ ▟█▙
//▐▌█▐▌ ▘▄▟▌ ▐▌   █▘    █   ▐█▌      ▐▛  ▘ ▐▌  ▐▛ ▜▌▐▛ ▐▌▐▙▄▟▌
//▐▌█▐▌▗█▀▜▌ ▐▌   █     █   ▗█▖      ▐▌    ▐▌  ▐▌ ▐▌▐▌ ▐▌▐▛▀▀▘
//▐▌█▐▌▐▙▄█▌ ▐▙▄  █   ▗▄█▄▖ ▟▀▙      ▝█▄▄▌ ▐▙▄ ▝█▄█▘▐▌ ▐▌▝█▄▄▌
//▝▘▀▝▘ ▀▀▝▘  ▀▀  ▀   ▝▀▀▀▘▝▀ ▀▘      ▝▀▀   ▀▀  ▝▀▘ ▝▘ ▝▘ ▝▀▀
//                              ▀▀▀▀▀
//A c script by Christos Angelopoulos, June 2022
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

int main() {
//get terminal lines and columns
struct winsize w;
ioctl(0, TIOCGWINSZ, &w);
int x,y,loop;
int row=48,col=190; //max values
int DART[row][col];
//printf("\e[?25l"); //hide cursor
//inf loop
loop=1;
while ( loop>0 )
{usleep(60000);
//You can change the speed of the letter cascade changing the value in the command above
	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	if ( row != w.ws_row || col != w.ws_col )
	{for (x=0;x<row;x++)	{for (y=0;y<col;y++)	{DART[x][y]=32;}}//fill matrix with white spaces
		system("clear");
		usleep(50000);
		row=w.ws_row;
		col=w.ws_col;
		int DART[row][col];
		//draw first linew.ws_col
		for (y=0;y<col;y++)
			{	DART[0][y]=rand() % 90 + 97;
			//the higher the 1st value gets, more spaces appear
			if (DART[0][y]>122) {DART[0][y]=32;}
		}
	} //if
	for (x=row-1;x>0;x--)
	{
			for (y=0;y<w.ws_col;y++)
			{
			if ( DART[x][y] == 32 && DART[x-1][y] != 32 && DART[x-2][y] == 32 )
					{DART[x-1][y]=32;}//get rid of single letters
			if ( DART[x][y] == 32 && DART[x-1][y] != 32)
						{DART[x][y]=rand() % 26 + 97;}
			else
					{ if ( DART[x][y] != 32 && DART[x-1][y] == 32)
						{DART[x][y]=32;}
						}
			}
	}
//rewrite first line
for (y=0;y<w.ws_col;y++)
{
if ( DART[0][y] == 32 ) {DART[0][y]=rand() % 550 + 97;}
else {DART[0][y]=rand() % 30 + 97;}

	if (DART[0][y]>122) {DART[0][y]=32;}
	}
//print matrix
system("clear");
for (x=0;x+1<w.ws_row;x++)
	{	for (y=0;y<w.ws_col;y++)
		{	if (DART[x][y] != 32 && DART[x+1][y] ==32 )
					{printf("\x1B[1m\x1b[33m%c\x1B[0m",DART[x][y]);}
				else {printf("\x1b[32m%c",DART[x][y]);}
/*You can change the colors to your liking:
RED "\x1b[31m"
GREEN "\x1b[32m"
YELLOW "\x1b[33m"
BLUE "\x1b[34m"
MAGENTA "\x1b[35m"
CYAN "\x1b[36m"
RESET "\x1b[0m" */
		}
 printf("\n");
	}
} //inf loop
return 0;
}
